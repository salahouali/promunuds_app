// =========================================================
// * Vuetify Material Dashboard - v2.1.0
// =========================================================
//
// * Product Page: https://www.creative-tim.com/product/vuetify-material-dashboard
// * Copyright 2019 Creative Tim (https://www.creative-tim.com)
//
// * Coded by Creative Tim
//
// =========================================================
//
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
// import VueToast from 'vue-toast-notification'
import Notifications from 'vue-notification'
// Import any of available themes
// import 'vue-toast-notification/dist/theme-sugar.css'
import './plugins/base'
import './plugins/chartist'
import './plugins/vee-validate'
import vuetify from './plugins/vuetify'
import i18n from './i18n'
import * as VueGoogleMaps from 'vue2-google-maps'
import wb from './registerServiceWorker'
import axios from 'axios'
import JsonExcel from 'vue-json-excel'

import VueHtml2Canvas from 'vue-html2canvas';
Vue.use(VueHtml2Canvas);


import firebase from 'firebase/app'
import 'firebase/messaging'
// Vue.use(VueToast)
// Vue.use(VueToast)
Vue.use(Notifications)

// Vue.$toast.open('You did it!')

var config = {
  apiKey: 'AIzaSyBq7EPfYKSSZz6fcxcwKWNrmf8u5gqb4mA',
  authDomain: 'promundusapp.firebaseapp.com',
  databaseURL: 'https://promundusapp.firebaseio.com',
  projectId: 'promundusapp',
  storageBucket: 'promundusapp.appspot.com',
  messagingSenderId: '1043722405147',
  appId: '1:1043722405147:web:7b49860a6ea33d81287fbe',
  measurementId: 'G-0J5GLQG93D',
} // 4. Get Firebase Configuration
firebase.initializeApp(config)

// const messaging = firebase.messaging()
if (firebase.messaging.isSupported()) {
  var messaging = firebase.messaging()
  messaging.getToken({ vapidKey: 'BCGJ3SJGqkJTM7t_yr2muEy33EiOtesepzT_nCHBO8RPRg7Mhk-0Op6psK4Mfmoygw2o-Dx7SzmqNbBQ5K7YI4A' })
  .then((token) => {
    // console.log(store.state)
    console.log(token)
    sendTokenToServer(token)
  })
 function sendTokenToServer (token) {
    if (!isTokenSentToServer()) {
      // console.log('Sending token to server...')
      // TODO(developer): Send the current token to your server.

      setTokenSentToServer(true)
    }
    }
    function isTokenSentToServer () {
    return window.localStorage.getItem('sentToServer') === '1'
    }
      function setTokenSentToServer (sent) {
    window.localStorage.setItem('sentToServer', sent ? '1' : '0')
     }
// Request Permission of Notifications
messaging.requestPermission().then(() => {
  // console.log('Notification permission granted.')

  // Get Token
  messaging.getToken().then((token) => {
    // getSelectedCheckboxes(token)
    var tokenAlreadyExist = false
    console.log(store.state.token.tokens)
    store.state.token.tokens.forEach(el => {
      if (el.token === token) {
        tokenAlreadyExist = true
      }
    })
    if (tokenAlreadyExist == false) {
      axios
        .post('https://application.promundus.fr:3003/api/fmcToken/newToken', {
          token: token,
        })
        .then(response => {
          return response.message
        })
    } else {
      console.log('Token already exist')
    }
  })
  messaging.onMessage(function (payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload)
  Vue.notify({
  group: 'foo',
  title: payload.notification.title,
  text: payload.notification.body,
})
})
})
} else {
  alert('no')
}
// messaging.usePublicVapidKey('BCGJ3SJGqkJTM7t_yr2muEy33EiOtesepzT_nCHBO8RPRg7Mhk-0Op6psK4Mfmoygw2o-Dx7SzmqNbBQ5K7YI4A') // 1. Generate a new key pair

// function getSelectedCheckboxes (token) {
//   // Create a dummy input to copy the string array inside it
//   var dummy = document.createElement('textarea')
//   // Add it to the document
//   dummy.value = token
//   document.body.appendChild(dummy)
//   // Set its ID
//   dummy.setAttribute('id', 'dummy_id')
//   // Output the array into it
//   // Select it
//   dummy.select()
//     // Copy its contents
//   document.execCommand('copy')
//   // Remove it as its not needed anymore
//   // document.body.removeChild(dummy)
// }
Vue.component('downloadExcel', JsonExcel)

Vue.config.productionTip = false

Vue.prototype.$workbox = wb
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBq7EPfYKSSZz6fcxcwKWNrmf8u5gqb4mA',
    libraries: 'places', // necessary for places input
  },
})

new Vue({
  router,
  store,
  vuetify,
  i18n,
  VueGoogleMaps,
  render: h => h(App),
}).$mount('#app')
