import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class TokenService {
  getAll () {
    return axios
      .get(API_URL + 'fmcToken/')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.tokens
      })
  }
}

export default new TokenService()
