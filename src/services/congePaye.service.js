import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/congepaye/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/modules/'

class CongePayeService {
  newConge (conge) {
    return axios
      .post(API_URL + 'nd', {
        userId: conge.userId,
        userNom: conge.userNom,
        userPrenom: conge.userPrenom,
        date: conge.date,
        dates: conge.dates,
        raison: conge.raison,
        nbjour: conge.nbjour,
        contact : conge.contact
      })
      .then(response => {
        return response.message
      })
  }

  getAllDcp () {
    return axios
      .get(API_URL)
      .then(response => {
        return response.data.dCongePaye
      })
  }

  getAllDcpArchived () {
    return axios
      .get(API_URL + 'archive')
      .then(response => {
        return response.data.conge
      })
  }

  deleteOne (conge) {
    return axios
      .post(API_URL + conge + '/archive')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        console.log(response.data)
        return response.data
      })
  }
}

export default new CongePayeService()
