import axios from 'axios'
var test = require('./services.env.js')

const API_URL = test.default + '/api/users/'

class UserService {
  getAll () {
    return axios
      .get(API_URL)
      .then(response => {
        return response.data.users
      })
  }

  newUser (user) {
    return axios
      .post(API_URL + 'signup', {
        nom: user.nom,
        prenom: user.prenom,
        password: user.password,
        identifiant: user.identifiant,
        entreprise : user.entreprise,
        contact : user.contact,
        client : user.client,
        lvl: user.lvl,
      })
      .then(response => {
        return response.message
      })
  }
}

export default new UserService()
