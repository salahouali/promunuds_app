import axios from 'axios'
var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class AuthService {
  login (user) {
    return axios
      .post(API_URL + 'users/login', {
        identifiant: user.identifiant,
        password: user.password,
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data))
        }

        return response.data
      })
  }

  updateUserPassword (payload) {
    return axios
    .post(API_URL + 'users/' + payload.userId + '/updateUserPassword', {
      password: payload.password,
    })
    .then(response => {
      return response.data
    })
  }

  updateUser (payload) {
    return axios
    .post(API_URL + 'users/' + payload.id + '/updateUser', {
      password: payload.password,
      identifiant: payload.identifiant,
      nom: payload.nom,
      prenom: payload.prenom,
      level: payload.lvl,
      contact : payload.contact
    })
    .then(response => {
      return response.data
    })
  }

  logout () {
    localStorage.removeItem('user')
  }
}

export default new AuthService()
