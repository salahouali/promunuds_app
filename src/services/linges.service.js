import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class LingeService {
  getAll () {
    return axios
      .get(API_URL + 'linges/')
      .then(response => {
        if (response.data) {
            return response.data.linges
        }
      })
  }
}

export default new LingeService()
