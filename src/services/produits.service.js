import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class ProduitService {
  getAll () {
    return axios
      .get(API_URL + 'produits/')
      .then(response => {
        console.log(response.data)
        if (response.data) {
          console.log(response.data.produits)
            return response.data.produits
        }
      })
  }
}

export default new ProduitService()
