import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class IncidentService {
  getAll () {
    return axios
      .get(API_URL + 'incident/getAll')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.incidents
      })
  }

  getAllArchived () {
    return axios
      .get(API_URL + 'incident/getAllArchived')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.incidents
      })
  }

  deleteOne (incident) {
    return axios
      .post(API_URL + 'incident/' + incident + '/archive')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        console.log(response.data)
        return response.data
      })
  }
}

export default new IncidentService()
