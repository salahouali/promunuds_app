import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/currentMission/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/modules/'

class CurrentMissionServices {
  newMission (mission) {
    return axios
      .post(API_URL + 'newMission', {
        userId: mission.userId,
        date: mission.date,
        posLat: mission.posLat,
        posLon: mission.posLon,
        hDebut: mission.hDebut,
        type: mission.type,
        idChantier: mission.idChantier,
        pProcess: mission.pProcess,
      })
      .then(response => {
        return response.message
      })
  }

  getCurrentMission (payload) {
    return axios
      .get(API_URL + payload.userId)
      .then(response => {
        return response.data.currentMission
      })
  }

  deleteOne (payload) {
    return axios
      .post(API_URL + payload.userId + '/delete')
      .then(response => {
        return response.data.message
      })
  }
}

export default new CurrentMissionServices()
