import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/remplacements/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/modules/'

class RemplacementsServices {
  newRemplacement (remplacement) {
    return axios
      .post(API_URL + 'nr', {
        userId: remplacement.userId,
        userNom: remplacement.userNom,
        userPrenom: remplacement.userPrenom,
        date: remplacement.date,
        hDebut: remplacement.hDebut,
        hFin: remplacement.hFin,
        idChantier: remplacement.idChantier,
      })
      .then(response => {
        return response.message
      })
  }

  userTakeRemplacement (payload) {
    return axios
      .post(API_URL + 'pov', {
        drId: payload.idRemplacement,
        userId: payload.userId,
        userPrenom: payload.userPrenom,
        userNom: payload.userNom,
      })
      .then(response => {
        return response.message
      })
  }

  getAllDr () {
    return axios
      .get(API_URL)
      .then(response => {
        return response.data.remplacements
      })
  }

  getAllDrArchived () {
    return axios
      .get(API_URL + 'archive')
      .then(response => {
        return response.data.remplacements
      })
  }

  getAllDrListeUser () {
    return axios
      .get(API_URL + 'getDrListUser')
      .then(response => {
        return response.data.drListeUser
      })
  }

  deleteOne (remplacement) {
    return axios
      .post(API_URL + remplacement + '/archive')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        console.log(response.data)
        return response.data
      })
  }
}

export default new RemplacementsServices()
