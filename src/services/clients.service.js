import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class ClientsService {
  getAll () {
    return axios
      .get(API_URL + 'clients/')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.clients
      })
  }

  addNew (client) {
    return axios
      .post(API_URL + 'clients/addClient', {
        id: client.id,
        name: client.name,
        adresse: client.adresse,
        lat: client.lat,
        lng: client.lng,
      })
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.client
      })
  }

  updateClient (client) {
    return axios
      .post(API_URL + 'clients/' + client._id + '/updateClient', {
        id: client.id,
        name: client.name,
        adresse: client.adresse,
        lat: client.lat,
        lng: client.lng,
      })
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.client
      })
  }

  deleteOne (client) {
    return axios
      .post(API_URL + 'clients/' + client + '/deleteOne')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.client
      })
  }
}

export default new ClientsService()
