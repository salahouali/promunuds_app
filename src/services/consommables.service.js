import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/'

class ConsommableService {
  getAll () {
    return axios
      .get(API_URL + 'consommables/')
      .then(response => {
        if (response.data) {
            return response.data.consommables
        }
      })
  }
}

export default new ConsommableService()
