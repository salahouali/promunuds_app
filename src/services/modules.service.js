import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/modules/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/modules/'

class ModulesServices {
  newMission (mission) {
    return axios
      .post(API_URL + 'newMission', {
        userId: mission.userId,
        userNom: mission.userNom,
        userPrenom: mission.userPrenom,
        date: mission.date,
        posLat: mission.posLat,
        posLon: mission.posLon,
        hDebut: mission.hDebut,
        hFin: mission.hFin,
        type: mission.type,
        idChantier: mission.idChantier,
        tempsTravailDecimal: mission.tempsTravailDecimal,
      })
      .then(response => {
        return response.message
      })
  }

  getAll () {
    return axios
      .get(API_URL)
      .then(response => {
        return response.data.missions
      })
  }
}

export default new ModulesServices()
