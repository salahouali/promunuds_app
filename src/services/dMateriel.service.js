import axios from 'axios'

var test = require('./services.env.js')

const API_URL = test.default + '/api/dmateriel/'
// const API_URL = 'https://444fc0850448.ngrok.io/api/modules/'

class RemplacementsServices {
  newDemande (payload) {
    return axios
      .post(API_URL + 'nd', {
        userId: payload.userId,
        userNom: payload.userNom,
        userPrenom: payload.userPrenom,
        date: payload.date,
        items: payload.items,
        idChantier: payload.idChantier,
        urgent: payload.urgent,
      })
      .then(response => {
        return response.message
      })
  }

  getAll () {
    return axios
      .get(API_URL)
      .then(response => {
        return response.data.dmateriels
      })
  }

  getAllDmArchived () {
    return axios
      .get(API_URL + 'archive')
      .then(response => {
        return response.data.dmateriels
      })
  }

  deleteOne (dm) {
    return axios
      .post(API_URL + dm + '/archive')
      .then(response => {
        if (response.data) {
          // localStorage.setItem('clients', response.data.clients.toString())
        }
        return response.data.client
      })
  }
}

export default new RemplacementsServices()
