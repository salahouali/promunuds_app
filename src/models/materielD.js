export default class MaterielD {
    constructor (date, userId, userNom, userPrenom, idChantier, items, urgent) {
      this.date = date
      this.items = items
      this.urgent = urgent
      this.userId = userId
      this.userNom = userNom
      this.userPrenom = userPrenom
      this.idChantier = idChantier
    }
  }
