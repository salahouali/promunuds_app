export default class RemplacementD {
    constructor (date, hDebut, hFin, userId, userNom, userPrenom, idChantier) {
      this.date = date
      this.hDebut = hDebut
      this.hFin = hFin
      this.userId = userId
      this.userNom = userNom
      this.userPrenom = userPrenom
      this.idChantier = idChantier
    }
  }
