export default class CongePaye {
    constructor (date, userId, userNom, userPrenom, dates, raison, nbjour) {
      this.date = date
      this.userId = userId
      this.userNom = userNom
      this.userPrenom = userPrenom
      this.dates = dates
      this.raison = raison
      this.nbjour = 0
    }
  }
