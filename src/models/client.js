export default class Client {
    constructor (id, name, adresse, lat, lng, _id) {
      this.id = id
      this.name = name
      this.adresse = adresse
      this.lat = lat
      this.lng = lng
      this._id = _id
    }
  }
