export default class User {
    constructor (identifiant, password, nom, prenom, lvl, contact, client, id) {
      this.identifiant = identifiant
      this.password = password
      this.nom = nom
      this.prenom = prenom
      this.lvl = lvl
      this.contact = contact
      this.client = client
      this.id = id
    }
  }
