import Vue from 'vue'
import Router from 'vue-router'
import Login from './views/dashboard/pages/Login.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      component: () => import('@/views/dashboard/Index'),
      children: [
        // Dashboard
        {
          name: 'Dashboard',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'User Profile',
          path: 'pages/user',
          component: () => import('@/views/dashboard/pages/UserProfile'),
        },
        {
          name: 'QR-CODE Gen',
          path: 'pages/qr_code_gen',
          component: () => import('@/views/dashboard/pages/QRCODEgen'),
        },
        {
          name: 'Modules',
          path: 'pages/all_modules',
          component: () => import('@/views/dashboard/pages/all_modules'),
        },
        {
          name: 'Module_RH',
          path: 'pages/rh',
          component: () => import('@/views/dashboard/pages/rh'),
        },
        {
          name: 'Liste relevé d\'heures',
          path: 'pages/lrh',
          component: () => import('@/views/dashboard/pages/lrh'),
        },
        {
          name: 'Module_clients',
          path: 'pages/clients',
          component: () => import('@/views/dashboard/pages/clients'),
        },
        {
          name: 'Formulaire demande de matériel - PMP',
          path: 'pages/dm_form_pmp.vue',
          component: () => import('@/views/dashboard/pages/dm_form_pmp'),
        },
        {
          name: 'Formulaire demande de matériel - Consommables sanitaire',
          path: 'pages/dm_form_conso.vue',
          component: () => import('@/views/dashboard/pages/dm_form_conso'),
        },
        {
          name: 'Formulaire demande de remplacement',
          path: 'pages/dr_form.vue',
          component: () => import('@/views/dashboard/pages/dr_form'),
        },
        {
          name: 'Les utilisateurs',
          path: 'pages/utilisateurs',
          component: () => import('@/views/dashboard/pages/users'),
        },
        {
          name: 'Liste des demandes de remplacement',
          path: 'pages/ldr_form',
          component: () => import('@/views/dashboard/pages/ldr'),
        },
        {
          name: 'Rapport incident',
          path: 'pages/rapport_incident',
          component: () => import('@/views/dashboard/pages/rapport_incident'),
        },
        {
          name: 'Liste rapport incident',
          path: 'pages/liste_rapport_incident',
          component: () => import('@/views/dashboard/pages/liste_rapport_incident'),
        },
        {
          name: 'Demande de congé payé',
          path: 'pages/demande_congé_payé',
          component: () => import('@/views/dashboard/pages/demande_conge_paye'),
        },
        {
          name: 'Liste demande de congé payé',
          path: 'pages/liste_demande_congé_payé',
          component: () => import('@/views/dashboard/pages/liste_demande_conge_paye'),
        },
        {
          name: 'Archives',
          path: 'pages/archives',
          component: () => import('@/views/dashboard/pages/archives'),
        },
        {
          name: 'Liste arrêt de travail',
          path: 'pages/liste_arret_travail',
          component: () => import('@/views/dashboard/pages/liste_arret_travail'),
        },
        {
          name: 'Liste demandes de matériel',
          path: 'pages/ldm.vue',
          component: () => import('@/views/dashboard/pages/ldm'),
        }        
      ],
    },
    {
      name: 'Login',
      path: '/login',
      component: Login,
    },
  ],
})
