import MaterielsService from '../services/materiels.service'

// const localsClients = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const materiels = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return MaterielsService.getAll().then(
        materiels => {
          commit('retrieveSuccess', materiels)
          return Promise.resolve(materiels)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, materiels) {
      state.materiels = materiels
    },
  },
}
