
import DMaterielService from '../services/dMateriel.service'

// const localsremplacements = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const dMateriel = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return DMaterielService.getAll().then(
        dMateriel => {
          commit('retrieveSuccess', dMateriel)
          return Promise.resolve(dMateriel)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAllArchived ({ commit }, payload) {
      return DMaterielService.getAllDmArchived(payload).then(
        dmateriel => {
          console.log(dmateriel)
          commit('retrieveSuccessArchived', dmateriel)
          return Promise.resolve(dmateriel)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    newDemande ({ commit }, payload) {
      return DMaterielService.newDemande(payload).then(
        dMateriel => {
          return Promise.resolve(dMateriel)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    deleteOne ({ commit }, payload) {
      return DMaterielService.deleteOne(payload).then(
        dm => {
          // commit('addClientToState', client)
          return Promise.resolve(dm)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, payload) {
      state.dMateriel = payload
    },
    uTRsuccess (state, payload) {
      // const test = state.remplacements.find(dRemplacement => dRemplacement._id === payload) //  .then(dRemplacement => {})
      // delete state.remplacements.indexOf(test)

    },
    retrieveSuccessArchived (state, payload) {
      state.archived = payload
    },
  },
}
