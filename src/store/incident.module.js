
import IncidentService from '../services/incident.service'

export const incident = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return IncidentService.getAll().then(
        incidents => {
          commit('retrieveSuccess', incidents)
          return Promise.resolve(incidents)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAllArchived ({ commit }) {
      return IncidentService.getAllArchived().then(
        incidents => {
          commit('retrieveSuccessArchived', incidents)
          return Promise.resolve(incidents)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    archive ({ commit }, incident) {
      return IncidentService.deleteOne(incident).then(
        incident => {
          commit('archivageSuccess', incident.incident)
          console.log(indident)
          return Promise.resolve(incident)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, incidents) {
      state.incident = incidents
    },
    retrieveSuccessArchived (state, incidents) {
      state.archivedIncident = incidents
    },
    archivageSuccess (state, incident) {
      console.log('here')
      // console.log(state.incident.indexOf(incident))
      // state.incident = state.incident.filter(function(el) { return el._id != incident });
      const test = state.incident.find(x => x._id === incident)
      console.log(test)
      delete state.incident.indexOf(test)
    },
  },
}
