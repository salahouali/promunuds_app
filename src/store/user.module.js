
import UserService from '../services/user.service'

export const users = {
  namespaced: true,
  state: {
  },
  actions: {
    newUser ({ commit }, user) {
      return UserService.newUser(user).then(
        mission => {
          return Promise.resolve(user)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAll ({ commit }) {
      return UserService.getAll().then(
        users => {
          commit('retrieveSuccess', users)
          return Promise.resolve(users)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, missions) {
      state.users = missions
    },
  },
}
