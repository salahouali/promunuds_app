
import ModulesService from '../services/modules.service'

export const modules = {
  namespaced: true,
  state: {
    userMissions: [],
  },
  actions: {
    newMission ({ commit }, mission) {
      return ModulesService.newMission(mission).then(
        mission => {
          return Promise.resolve(mission)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAll ({ commit }) {
      return ModulesService.getAll().then(
        missions => {
          commit('retrieveSuccess', missions)
          return Promise.resolve(missions)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, missions) {
      state.listeMissions = missions
    },
    addMissionsOfUser (state, missions) {
      state.userMissions = missions
    },
  },
}
