import ProduitsService from '../services/produits.service'

// const localsClients = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const produits = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return ProduitsService.getAll().then(
        produits => {
          commit('retrieveSuccess', produits)
          return Promise.resolve(produits)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, produits) {
      state.produits = produits
    },
  },
}
