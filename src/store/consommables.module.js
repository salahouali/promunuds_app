import ConsommableService from '../services/consommables.service'

// const localsClients = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const consommables = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return ConsommableService.getAll().then(
        consommables => {
          commit('retrieveSuccess', consommables)
          return Promise.resolve(consommables)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, consommables) {
      state.consommables = consommables
    },
  },
}
