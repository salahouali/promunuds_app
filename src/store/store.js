import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './auth.module'
import { modules } from './modules.module'
import { clients } from './clients.module'
import { remplacements } from './remplacements.module'
import { currentMission } from './currentMission.module'
import { users } from './user.module'
import { incident } from './incident.module'
import { token } from './token.module'
import { consommables } from './consommables.module'
import { linges } from './linges.module'
import { materiels } from './materiels.module'
import { produits } from './produits.module'
import { dMateriel } from './dMateriel.module'
import { congePaye } from './congePaye.module'

import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)

export default new Vuex.Store({
  // Persistance du store
  plugins: [createPersistedState({
    storage: window.sessionStorage,
    })],
  state: {
    liste_remplacements: {
      remplacements: {
        _id: 'Default',
        date: 'Default',
        hDebut: '',
        hFin: '',
        idChantier: '',
        userId: '',
        userNom: '',
        userPrenom: '',
      },
    },
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    drawer: null,
    mission: {
      type: null,
      hDebut: null,
      hFin: null,
      date: null,
      posLat: null,
      posLon: null,
      idChantier: 'null',
      userId: null,
      userNom: null,
      userPrenom: null,
      tempsTravailDecimal: null,
      resultInputManu: '',
    },
    processLvl: {
      dr: 1,
      cp: 1,
      incident: 1,
      pmp: 1,
      is: 1,
    },
    cpProcess: {
      date: null,
    },
    drProcess: {
      time: null,
      time2: null,
      date: null,
      idChantier: null,
    },
    marker: { lat: '', lng: '' },
    pProcess: 1,
    snackbar: {
      show: false,
      message: '',
    },
    modules: {
      remplacements: {
      },
      userMissions: [],
    },
    userClient : {
      clients : []
    },
    currentDrVal: {},
    drListeUser: {},
    api_url: 'https://application.promundus.fr:3003', // 'http://localhost:3003',
  },
  mutations: {
    SET_BAR_IMAGE (state, payload) {
      state.barImage = payload
    },
    SET_DRAWER (state, payload) {
      state.drawer = payload
    },
    updateMission (state, { key, value }) {
      state.mission[key] = value
    },
    updateUserClient (state, payload){
      state.userClient.clients.push(payload)
    },
    setMission (state, mission) {
      state.mission = mission
    },
    cancelMission (state) {
      for (const [key] of Object.entries(state.mission)) {
        state.mission[key] = null
      }
      if (state.currentMission.currentMission !== undefined) {
        for (const [key] of Object.entries(state.currentMission.currentMission)) {
          state.currentMission.currentMission[key] = null
        }
      }
    },
    updatePProcess (state, payload) {
      state.pProcess = payload
    },
    newSnackBar (state, payload) {
      state.snackbar.show = true
      state.snackbar.message = payload
    },
    closeSnackBar (state) {
      state.snackbar.show = false
      state.snackbar.message = null
    },
    updateMarker (state, { lat, lng }) {
      state.marker.lat = lat
      state.marker.lng = lng
    },
    updateProcessLvl (state, { process, value }) {
      state.processLvl[process] = value
    },
    updateDrProcess (state, { key, value }) {
      state.drProcess[key] = value
    },
    updateCpProcess (state, { key, value }) {
      state.cpProcess[key] = value
    },
    storeCurrentDr (state, payload) {
      state.currentDrVal = payload
    },
    cancelDr (state) {
      for (const [key] of Object.entries(state.drProcess)) {
        state.drProcess[key] = null
      }
    },
  },
  actions: {

  },
  modules: {
    auth,
    modules,
    clients,
    remplacements,
    currentMission,
    users,
    incident,
    token,
    consommables,
    linges,
    materiels,
    produits,
    dMateriel,
    congePaye,
  },
})
