
import ClientsService from '../services/clients.service'

// const localsClients = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const clients = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return ClientsService.getAll().then(
        clients => {
          commit('retrieveSuccess', clients)
          return Promise.resolve(clients)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    addNew ({ commit }, client) {
      return ClientsService.addNew(client).then(
        client => {
          commit('addClientToState', client)
          return Promise.resolve(client)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    updateClient ({ commit }, client) {
      return ClientsService.updateClient(client).then(
        client => {
          // commit('addClientToState', client)
          return Promise.resolve(client)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    deleteOne ({ commit }, client) {
      return ClientsService.deleteOne(client).then(
        client => {
          // commit('addClientToState', client)
          return Promise.resolve(client)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, clients) {
      state.clients = clients
    },
    addClientToState (state, client) {
      state.clients.push(client)
    },
  },
}
