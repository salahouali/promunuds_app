
import CongePayeService from '../services/congePaye.service'

// const localsremplacements = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const congePaye = {
  namespaced: true,
  actions: {
    getAllDcp ({ commit }) {
      return CongePayeService.getAllDcp().then(
        congepaye => {
          commit('retrieveSuccess', congepaye)
          return Promise.resolve(congepaye)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAllDcpArchived ({ commit }) {
      return CongePayeService.getAllDcpArchived().then(
        congepaye => {
          commit('retrieveSuccessArchived', congepaye)
          return Promise.resolve(congepaye)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    newConge ({ commit }, congepaye) {
      return CongePayeService.newConge(congepaye).then(
        congepaye => {
          return Promise.resolve(congepaye)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    archive ({ commit }, congepaye) {
      return CongePayeService.deleteOne(congepaye).then(
        congepaye => {
          commit('archivageSuccess', congepaye.congepaye)
          console.log(congepaye)
          return Promise.resolve(congepaye)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, payload) {
      state.congepaye = payload
    },
    retrieveSuccessArchived (state, payload) {
      state.archived = payload
    },
    retrieveSuccessDrListeUser (state, payload) {
      state.drListeUser = payload
    },
    uTRsuccess (state, payload) {
      // const test = state.remplacements.find(dRemplacement => dRemplacement._id === payload) //  .then(dRemplacement => {})
      // delete state.remplacements.indexOf(test)

    },
  },
}
