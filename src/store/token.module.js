
import TokenService from '../services/token.service'

export const token = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return TokenService.getAll().then(
        tokens => {
          commit('retrieveSuccess', tokens)
          return Promise.resolve(tokens)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, tokens) {
      state.tokens = tokens
    },
  },
}
