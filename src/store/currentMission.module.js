
import currentMissionService from '../services/currentMission.service'

export const currentMission = {
  namespaced: true,
  state: {
  },
  actions: {
    newMission ({ commit }, mission) {
      return currentMissionService.newMission(mission).then(
        mission => {
            // commit('retrieveSuccess', mission)
          return Promise.resolve(mission)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getCurrentMission ({ commit }, payload) {
      return currentMissionService.getCurrentMission(payload).then(
        currentMission => {
          commit('retrieveSuccess', currentMission)
          return Promise.resolve(currentMission)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    deleteOne ({ commit }, payload) {
      return currentMissionService.deleteOne(payload).then(
        response => {
          return Promise.resolve(response)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, mission) {
      state.currentMission = mission
    },
    addMissionsOfUser (state, missions) {
      state.userMissions = missions
    },
  },
}
