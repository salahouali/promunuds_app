import LingesService from '../services/linges.service'

// const localsClients = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const linges = {
  namespaced: true,
  actions: {
    getAll ({ commit }) {
      return LingesService.getAll().then(
        linges => {
          commit('retrieveSuccess', linges)
          return Promise.resolve(linges)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, linges) {
      state.linges = linges
    },
  },
}
