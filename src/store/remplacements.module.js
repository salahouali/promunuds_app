
import RemplacementsService from '../services/remplacements.service'

// const localsremplacements = localStorage.getItem('clients')
// console.log(localStorage.getItem('clients'))
// const initialState = localsClients
//   ? { clientsHandler: { clientsList: true }, clients: JSON.parse(localsClients) }
//   : { clientsHandler: { clientsList: false }, clients: null }

export const remplacements = {
  namespaced: true,
  actions: {
    getAllDr ({ commit }) {
      return RemplacementsService.getAllDr().then(
          remplacements => {
          commit('retrieveSuccess', remplacements)
          return Promise.resolve(remplacements)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAllDrArchived ({ commit }) {
      return RemplacementsService.getAllDrArchived().then(
          remplacements => {
          commit('retrieveSuccessArchived', remplacements)
          return Promise.resolve(remplacements)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    getAllDrListeUser ({ commit }) {
      return RemplacementsService.getAllDrListeUser().then(
          drListeUser => {
          commit('retrieveSuccessDrListeUser', drListeUser)
          return Promise.resolve(drListeUser)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    userTakeRemplacement ({ commit }, payload) {
      return RemplacementsService.userTakeRemplacement(payload).then(
          res => {
          commit('uTRsuccess', payload.idRemplacement)
          // ! Mutation pour delete du store l'input
          return Promise.resolve(res)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    newRemplacement ({ commit }, remplacement) {
      return RemplacementsService.newRemplacement(remplacement).then(
        remplacementD => {
          return Promise.resolve(remplacementD)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
    archive ({ commit }, remplacement) {
      return RemplacementsService.deleteOne(remplacement).then(
        remplacement => {
          commit('archivageSuccess', remplacement.remplacement)
          console.log(remplacement)
          return Promise.resolve(remplacement)
        },
        error => {
          return Promise.reject(error)
        },
      )
    },
  },
  mutations: {
    retrieveSuccess (state, payload) {
      state.remplacements = payload
    },
    retrieveSuccessArchived (state, payload) {
      state.archived = payload
    },
    retrieveSuccessDrListeUser (state, payload) {
      state.drListeUser = payload
    },
    uTRsuccess (state, payload) {
      // const test = state.remplacements.find(dRemplacement => dRemplacement._id === payload) //  .then(dRemplacement => {})
      // delete state.remplacements.indexOf(test)

    },
  },
}
