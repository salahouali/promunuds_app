// Expressx
const express = require('express');
const app = express();

// Morgan (show logs)
const morgan = require('morgan');

// Body-parser
const bodyParser = require('body-parser');

// Mongoose
const mongoose = require('mongoose');

require("dotenv").config();

//---------

const userRoutes = require('./api/routes/userRoutes');
const modulesRoutes = require('./api/routes/modulesRoutes');
const clientRoutes = require('./api/routes/clientRoutes');
const remplacementD = require('./api/routes/remplacementDRoutes');
const currentMission = require('./api/routes/currentMissionRoutes');
const incidentRoutes = require('./api/routes/incidentRoutes');
const fmcToken = require('./api/routes/fmcTokenRoutes');

const consommableRoutes = require('./api/routes/consommableRoutes');
const lingeRoutes = require('./api/routes/lingeRoutes');
const materielRoutes = require('./api/routes/materielRoutes');
const produitRoutes = require('./api/routes/produitRoutes');
const dMaterielRoutes = require('./api/routes/dmaterielRoutes');
const congePayeRoutes = require('./api/routes/dCongePaye')





// MongoDB atlas connection

mongoose.set('debug', true);

  mongoose.connect(
     process.env.MONGO_URL,
     {
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
     }); 
     
    //  const uri = "mongodb+srv://salah:0fh3iEKBKqaXMNf1@cluster0.vu0ed.mongodb.net/<dbname>?retryWrites=true&w=majority";
    //  const client = new MongoClient(uri, { useNewUrlParser: true });
    //  client.connect(err => {
    //    const collection = client.db("test").collection("devices");
    //    // perform actions on the collection object
    //    client.close();
    //  });

// mongoose.connect(
//     'mongodb://localhost:27017/nostroflix',
//         {
//             useNewUrlParser: true,
//             useFindAndModify: false
//         });

//------------------ Middlewares ----------------

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: false}));

app.use(bodyParser.json());

// Handling CORS
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers', 
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );

    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods',
            'PUT, POST, DELETE, GET, PATCH'
        );
        return res.status(200).json({});
    }
    next();
});

// routes
app.use('/api/users', userRoutes);
app.use('/api/modules', modulesRoutes);
app.use('/api/clients', clientRoutes);
app.use('/api/remplacements', remplacementD)
app.use('/api/currentMission', currentMission)
app.use('/api/incident/', incidentRoutes)
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use('/api/fmcToken', fmcToken)
app.use('/api/consommables', consommableRoutes)
app.use('/api/produits', produitRoutes)
app.use('/api/materiels', materielRoutes)
app.use('/api/linges', lingeRoutes)
app.use('/api/dmateriel', dMaterielRoutes)
app.use('/api/congepaye', congePayeRoutes)
// Handling errors

app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

//------

module.exports = app;
