const express = require('express');
const router = express.Router();

const currentMissionController = require('../controllers/currentMissionController');

router.post('/newMission', currentMissionController.newMission);

router.get('/:userId', currentMissionController.getOne);

router.post('/:userId/delete', currentMissionController.deleteOne);

// router.get('/', currentMissionController.getAll);

// router.get('/missions/:id', currentMissionController.getAllOfUser);

module.exports = router;