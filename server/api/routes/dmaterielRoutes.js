const express = require('express');
const router = express.Router();

//---
const dmaterielController = require('../controllers/dMaterielController');

//---get all----
router.post('/nd', dmaterielController.newDemande);

router.get('/', dmaterielController.getAll);
router.get('/archive', dmaterielController.getAllArchived);


router.post('/:_id/archive', dmaterielController.archive);
module.exports = router;