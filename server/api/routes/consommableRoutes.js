const express = require('express');
const router = express.Router();

//---
const consommableController = require('../controllers/consommableController');

//---get all----
router.get('/', consommableController.getAll);

module.exports = router;