const express = require('express');
const router = express.Router();

//---
const materielController = require('../controllers/materielController');

//---get all----
router.get('/', materielController.getAll);

module.exports = router;