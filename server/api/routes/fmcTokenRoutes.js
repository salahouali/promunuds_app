const express = require('express');
const router = express.Router();

const fmcTokenController = require('../controllers/fmcTokenController');

router.post('/newToken', fmcTokenController.newToken);

router.get('/', fmcTokenController.getAll);

module.exports = router;