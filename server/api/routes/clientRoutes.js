const express = require('express');
const router = express.Router();

//---
const clientController = require('../controllers/clientController');

//---get all----
router.get('/', clientController.getAll);

//---get by id----
router.get('/:id', clientController.getOne);

router.get('/search/:input', clientController.search);

router.post('/addClient', clientController.addClient);
router.post('/:_id/updateClient', clientController.updateClient);
router.post('/:_id/deleteOne', clientController.deleteOne);


module.exports = router;