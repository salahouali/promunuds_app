const express = require('express');

const router = express.Router();

const multer = require('multer');
//---
const incidentController = require('../controllers/incidentController');

const storageImg = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/images/');
    },
    filename: function(req, file, cb) {
      console.log(file)
        // myFile = new Date().toISOString() + '_' + file.originalname; 
         myFile = file.originalname; 
        cb(null, myFile.replace(/:/g,'_'));
    }
  });

const uploadImg = multer({
  storage: storageImg
});


router.post('/new/', uploadImg.single('file') , incidentController.new);
router.post('/upload/', uploadImg.single('file') , incidentController.uploadImg);
router.post('/:_id/archive', incidentController.archive);

router.get('/getAll/', incidentController.getAll);
router.get('/getAllArchived/', incidentController.getAllArchived);
router.post('/:name/deleteImg', incidentController.deleteImg)

module.exports = router;