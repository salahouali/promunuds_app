const express = require('express');
const router = express.Router();

//---
const clientController = require('../controllers/remplacementDController');

//---get all----
router.get('/', clientController.getAllDr);
router.get('/archive', clientController.getAllDrArchived);

router.get('/getDrListUser', clientController.getAllDrListeUser);

//---get by id----
// router.get('/:id', clientController.getOne);

// router.get('/search/:input', clientController.search);

router.post('/nr', clientController.newRemplacement);

router.post('/pov', clientController.pushOneVolontaire);
router.post('/:_id/archive', clientController.archive);

module.exports = router;