const express = require('express');
const router = express.Router();

//---
const dCongePayeController = require('../controllers/dCongePayeController');

//---get all----
router.post('/nd', dCongePayeController.newDemande);

router.get('/', dCongePayeController.getAll);
router.get('/archive', dCongePayeController.getAllArchived);


router.post('/:_id/archive', dCongePayeController.archive);
module.exports = router;