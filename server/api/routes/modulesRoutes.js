const express = require('express');
const router = express.Router();

const modulesController = require('../controllers/modulesController');

router.post('/newMission', modulesController.newMission);

router.get('/', modulesController.getAll);

router.get('/missions/:id', modulesController.getAllOfUser);

module.exports = router;