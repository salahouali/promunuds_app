const express = require('express');
const router = express.Router();

//---
const lingeController = require('../controllers/lingeController');

//---get all----
router.get('/', lingeController.getAll);

module.exports = router;