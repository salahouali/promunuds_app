const express = require('express');
const router = express.Router();

//---
const produitController = require('../controllers/produitController');

//---get all----
router.get('/', produitController.getAll);

module.exports = router;