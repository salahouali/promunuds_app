const Linge = require('../models/linge');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await Linge.find()
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          linges: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
};