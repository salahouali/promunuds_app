const DMateriel = require('../models/dMateriel');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await DMateriel.find({ archive : "false"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          dmateriels: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },

  getAllArchived: async (req, res, next) => {
    await DMateriel.find({ archive : "true"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          dmateriels: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },

  newDemande: async (req, res, next) => {
    const date =   req.body.date;
    const items = req.body.items;
    const userId =   req.body.userId;
    const userNom =   req.body.userNom;
    const userPrenom =   req.body.userPrenom;
    const idChantier =   req.body.idChantier;
    const urgent =   req.body.urgent;
    const archive = "false"
    console.log(req.body)
    const dMateriel = new DMateriel({
        _id: new mongoose.Types.ObjectId(),                
        date : date,
        userId : userId,
        userNom : userNom,
        userPrenom : userPrenom,
        idChantier : idChantier,
        items : items,
        urgent : urgent,
        archive : archive,

    });
    dMateriel
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Nouvelle demande de matériel ajoutée !'
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
  },

  archive: async (req, res, next) => {
    DMateriel.updateOne({_id: req.params._id}, { $set : { "archive" : true}})
    .exec()
    .then(data => {
      console.log(data);
      res.status(200).json({
        message: 'Dm archived',
    });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
        error: err
    });
      });
  },
};