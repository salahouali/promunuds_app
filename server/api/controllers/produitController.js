const Produit = require('../models/produit');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await Produit.find()
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          produits: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
};