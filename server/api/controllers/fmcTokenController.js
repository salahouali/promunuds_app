const FmcToken = require('../models/fmcToken')

const mongoose = require('mongoose');

// Module RH
module.exports = {
    //Création post
        newToken: async (req, res, next) => {
            const token =   req.body.token;

            const fmcToken = new FmcToken({
                token: token,                
            });
            fmcToken
                .save()
                .then(result => {
                    res.status(201).json({
                        message: 'Nouveau token ajouté !'
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
        },
        getAll: async (req, res, next) => {
            await FmcToken.find()
              .select()
              .exec()
              .then(docs => {
                const response = {
                  count: docs.length,
                  tokens: docs
                };
                res.status(200).json(response);
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          },    
    };

// Module RH FIN