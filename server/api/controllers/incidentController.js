const Incident = require('../models/incident');
var fs = require('fs');

//---
const mongoose = require('mongoose');

module.exports = {
    uploadImg: async (req, res, next) => {
    res.status(201).json({
                message: 'new incident',
            });
    },
    new: async (req, res, next) => {
      const incident = new Incident({
          _id: new mongoose.Types.ObjectId(),
          image : req.body.images,                
          details : req.body.details,
          localisation : req.body.localisation,
          idChantier : req.body.idChantier,
          prenom : req.body.prenom,
          nom : req.body.nom,
          date : req.body.date,
          archive : false,
      });
      incident.save()
      .then(result => {
          res.status(201).json({
              message: 'new incident',
          });  
      }).catch(err => {
          // console.log(err);
          console.log(req.file)
          res.status(500).json({
            error: err
          });
        });

  }, 
    getAll: async (req, res, next) => {
        await Incident.find({ archive: false})
          .select()
          .exec()
          .then(docs => {
            const response = {
              count: docs.length,
              incidents: docs
            };
            res.status(200).json(response);
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      },
      getAllArchived: async (req, res, next) => {
        await Incident.find({ archive: true})
          .select()
          .exec()
          .then(docs => {
            const response = {
              count: docs.length,
              incidents: docs
            };
            res.status(200).json(response);
          })
          .catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
      },
      deleteImg: async (req, res, next) => {
        console.log(req.params)
        var filePath = 'uploads\\images\\' + req.params.name; 
        if (fs.unlinkSync(filePath)) {
          res.status(201).json({
            message: 'Photo deleted',
        });  
        }
      },
      archive: async (req, res, next) => {
        Incident.updateOne({_id: req.params._id}, { $set : { "archive" : true}})
        .exec()
        .then(data => {
          console.log(data)
          res.status(200).json({
            message: 'Incident archived',
            incident : data
        });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
            error: err
        });
          });
      },
};






