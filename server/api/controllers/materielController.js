const Materiel = require('../models/materiel');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await Materiel.find()
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          materiels: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
};