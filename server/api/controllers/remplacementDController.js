const RemplacementD = require('../models/remplacementD');
const DrListeUser = require('../models/drListeUser');
//---
const mongoose = require('mongoose');

module.exports = {
  getAllDr: async (req, res, next) => {
    await RemplacementD.find({ archive : "false"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          remplacements: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
  getAllDrArchived: async (req, res, next) => {
    await RemplacementD.find({ archive : "true"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          remplacements: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
  getAllDrListeUser: async (req, res, next) => {
    await DrListeUser.find()
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          drListeUser: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
  pushOneVolontaire: async (req, res, next) => {
    const drId = req.body.drId
    const userId = req.body.userId
    const userPrenom = req.body.userPrenom
    const userNom = req.body.userNom
    await RemplacementD.updateOne({_id: drId}, { $inc : { nbVolontaire : 1}})
      .exec()
      .then(docs => {
        console.log(docs)
        const drListeUser = new DrListeUser({
          _id: new mongoose.Types.ObjectId(),  
          userId: userId,
          drId: drId,
          userPrenom: userPrenom,
          userNom: userNom,
        })
        drListeUser
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Utilisateur enregistré dans la table drListeUser'
            });
        }).catch(err => {
            // console.log(err);
            res.status(500).json({
              error: err
            });
          });
        // res.status(200).json({message : 'Element mis à jour'});
      })
      // .catch(err => {
      //   console.log(err);
      //   res.status(500).json({
      //     error: err
      //   });
      // });
  },
  // deleteOne: async (req, res, next) => {
  //   const id = req.body.id
  //   await RemplacementD.deleteOne( { "_id" : id } )
  //     .exec()
  //     .then(docs => {
  //       console.log(res)
  //       res.status(200).json({message : 'Element supprimé'});
  //     })
  //     .catch(err => {
  //       console.log(err);
  //       res.status(500).json({
  //         error: err
  //       });
  //     });
  // },
  newRemplacement: async (req, res, next) => {
    const date =   req.body.date;
    const hDebut =   req.body.hDebut;
    const hFin =   req.body.hFin;
    const userId =   req.body.userId;
    const userNom =   req.body.userNom;
    const userPrenom =   req.body.userPrenom;
    const idChantier =   req.body.idChantier;

    const remplacementD = new RemplacementD({
        _id: new mongoose.Types.ObjectId(),                
        date : date,
        hDebut : hDebut,
        hFin : hFin,
        userId : userId,
        userNom : userNom,
        userPrenom : userPrenom,
        idChantier : idChantier,
        nbVolontaire : 0,
        archive: "false"
    });
    remplacementD
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Nouvelle demande ajoutée'
            });
        }).catch(err => {
            // console.log(err);
            res.status(500).json({
              error: err
            });
          });
  },
  archive: async (req, res, next) => {
    RemplacementD.updateOne({_id: req.params._id}, { $set : { "archive" : true}})
    .exec()
    .then(data => {
      console.log(data)
      res.status(200).json({
        message: 'Demande de remplacement archived',
        incident : data
    });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
        error: err
    });
      });
  },  
};