const DCongePaye = require('../models/congePaye');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await DCongePaye.find({ archive : "false"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          dCongePaye: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },

  getAllArchived: async (req, res, next) => {
    await DCongePaye.find({ archive : "true"})
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          dCongePaye: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },

  newDemande: async (req, res, next) => {
    const date =   req.body.date;
    const userId =   req.body.userId;
    const userNom =   req.body.userNom;
    const userPrenom =   req.body.userPrenom;
    const raison =   req.body.raison;
    const nbjour =   req.body.nbjour;
    const dates = req.body.dates;
    const contact = req.body.contact;
    const archive = "false"
    console.log(req.body)
    const dCongePaye = new DCongePaye({
        _id: new mongoose.Types.ObjectId(),                
        date : date,
        userId : userId,
        userNom : userNom,
        userPrenom : userPrenom,
        raison : raison,
        dates : dates,
        archive : archive,
        nbjour : nbjour,
        contact : contact

    });
    dCongePaye
        .save()
        .then(result => {
            res.status(201).json({
                message: 'Nouvelle demande de matériel ajoutée !'
            });
        }).catch(err => {
            console.log(err);
            res.status(500).json({
              error: err
            });
          });
  },

  archive: async (req, res, next) => {
    DCongePaye.updateOne({_id: req.params._id}, { $set : { "archive" : true}})
    .exec()
    .then(data => {
      console.log(data);
      res.status(200).json({
        message: 'Dcp archived',
    });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
        error: err
    });
      });
  },
};