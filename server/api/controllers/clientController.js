const Client = require('../models/client');

//---
const mongoose = require('mongoose');

module.exports = {
  getAll: async (req, res, next) => {
    await Client.find()
      .select()
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          clients: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
  search: async (req, res, next) => {
    const input = req.params.input;
    await Client.find({ "id": { "$regex": input, "$options": "i" } })
      .select()
      .exec()
      .then(docs => {
        const response = {
          clients: docs
        };
        res.status(200).json(response);
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({
          error: err
        });
      });
  },
  updateClient: async (req, res, next) => {
      Client.updateOne({ _id : req.params._id }, {
        $set: {
          id: req.body.id,
          name: req.body.name,
          adresse: req.body.adresse,
          lat: req.body.lat,
          lng: req.body.lng 
        }
      })
      .exec()
  .then(data => {
    console.log(data);
    res.status(200).json({
      message: 'User updated',
  });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
        error: err
    });
      });
  },
  deleteOne: async (req, res, next) => {
    Client.deleteOne({ _id : req.params._id })
    .exec()
    .then(data => {
      console.log(data);
      res.status(200).json({
        message: 'User deleted',
    });
  })
  .catch(err => {
    console.log(err);
    res.status(500).json({
        error: err
    });
      });
  },
  getOne: async (req, res, next) => {
    const id = req.params.id;
    console.log(id)
    await Client.findById(id)
      .select("_id nom_usuel")
      .exec()
      .then(doc => {
        console.log("From database", doc);
        if (doc) {
          res.status(200).json({
            user: doc
          });
        } else {
          res.status(400).json({
            message: 'No valide entry found for provided ID'
          });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
      });
  },
  addClient: async (req, res, next) => {
    const nom_usuel =   req.body.id;
    const nom_client =   req.body.name;
    const adresse =   req.body.adresse;
    const lat =   req.body.lat;
    const lng =   req.body.lng;


    const client = new Client({
        _id: new mongoose.Types.ObjectId(),                
        id: nom_usuel,
        name: nom_client,
        adresse: adresse,
        lat : lat,
        lng : lng
    });
    console.log(client)
    client
        .save()
        .then(result => {
            res.status(201).json({
                message: 'client added',
                client: client,
            });
        }).catch(err => {
            // console.log(err);
            res.status(500).json({
              error: err
            });
          });
  },  
};