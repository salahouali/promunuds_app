const CurrentMission = require('../models/currentMission')

const mongoose = require('mongoose');

// Module RH
module.exports = {
    //Création post
        newMission: async (req, res, next) => {
            const userId =   req.body.userId;
            const posLat = req.body.posLat;
            const posLon = req.body.posLon;
            const date =   req.body.date;
            const hDebut =   req.body.hDebut;
            const type =   req.body.type;
            const idChantier =  req.body.idChantier;
            const pProcess =  req.body.pProcess;

            console.log(req.body)
            const currentMission = new CurrentMission({
                _id: new mongoose.Types.ObjectId(),                
                date: date,
                posLat: posLat,
                posLon : posLon,
                hDebut: hDebut,
                type: type,
                userId: userId,
                idChantier: idChantier,
                pProcess: pProcess,
            });
            currentMission
                .save()
                .then(result => {
                    res.status(201).json({
                        message: 'Nouvelle current mission ajouté !'
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
        },
        getOne: async (req, res, next) => {
          const userId = req.params.userId;
          console.log(userId)
          await CurrentMission.find({userId : userId})
            .select()
            .exec()
            .then(currentMission => {
              console.log("From database", currentMission);
              if (currentMission) {
                res.status(200).json({
                  currentMission : currentMission[0]
                });
              } else {
                res.status(400).json({
                  message: 'Aucune mission en cours pour cet utilisateur'
                });
              }
            })
            .catch(err => {
              console.log(err);
              res.status(500).json({ error: err });
            });
        },
        deleteOne: async (req, res, next) => {
          const userId = req.params.userId;
          console.log(userId)
          await CurrentMission.deleteOne({userId : userId})
            .select()
            .exec()
            .then(result => {
              res.status(200).json({
                  message: 'Current mission deleted'
              });
          })
          .catch(err => {
              console.log(err);
              res.status(500).json({
                  error: err
              });
          });
        },    
    };

// Module RH FIN