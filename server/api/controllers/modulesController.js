const Mission = require('../models/mission')

const mongoose = require('mongoose');

// Module RH
module.exports = {
    //Création post
        newMission: async (req, res, next) => {
            const userId =   req.body.userId;
            const userNom =   req.body.userNom;
            const userPrenom =   req.body.userPrenom;
            const posLat = req.body.posLat;
            const posLon = req.body.posLon;
            const date =   req.body.date;
            const hDebut =   req.body.hDebut;
            const hFin =   req.body.hFin;
            const type =   req.body.type;
            const idChantier =  req.body.idChantier;
            const tempsTravailDecimal = req.body.tempsTravailDecimal

            const mission = new Mission({
                _id: new mongoose.Types.ObjectId(),                
                date: date,
                posLat: posLat,
                posLon : posLon,
                hDebut: hDebut,
                hFin: hFin,
                type: type,
                userId: userId,
                userNom: userNom,
                userPrenom: userPrenom,
                idChantier: idChantier,
                tempsTravailDecimal: tempsTravailDecimal,
            });
            mission
                .save()
                .then(result => {
                    res.status(201).json({
                        message: 'Nouvelle mission ajouté !'
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).json({
                      error: err
                    });
                  });
        },
        getAll: async (req, res, next) => {
            await Mission.find()
              .select()
              .exec()
              .then(docs => {
                const response = {
                  count: docs.length,
                  missions: docs
                };
                res.status(200).json(response);
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({
                  error: err
                });
              });
          },
          getAllOfUser: async (req, res, next) => {
            const id = req.params.id;
            await Mission.find({userId : id})
              .select()
              .exec()
              .then(docs => {
                console.log("From database", docs);
                if (docs) {
                  res.status(200).json({
                    missions: docs
                  });
                } else {
                  res.status(400).json({
                    message: 'No valide entry found for provided ID'
                  });
                }
              })
              .catch(err => {
                console.log(err);
                res.status(500).json({ error: err });
              });
          },        
    };

// Module RH FIN