const mongoose = require('mongoose');

const missionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: { type: String, required: true },
    posLat: { type: Number, required: true },
    posLon: { type: Number, required: true },
    hDebut: { type: String, required: true },
    hFin: { type: String, required: true },
    type: { type: String, required: true },
    userId: { type : String, required : true},        
    userNom: { type : String, required : true},        
    userPrenom: { type : String, required : true},        
    idChantier: { type: String, required : true },
    tempsTravailDecimal : {type: Number, required: true}
});

module.exports = mongoose.model('mission', missionSchema);