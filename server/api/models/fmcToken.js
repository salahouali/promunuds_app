const mongoose = require('mongoose');

const fmcTokenSchema = mongoose.Schema({
    token: { type: String, required: true },
});

module.exports = mongoose.model('fmcToken', fmcTokenSchema);