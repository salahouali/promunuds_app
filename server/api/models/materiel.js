const mongoose = require('mongoose');

const materielSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nom: { type: String, required: true },
});

module.exports = mongoose.model('materiel', materielSchema);