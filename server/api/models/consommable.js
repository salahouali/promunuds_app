const mongoose = require('mongoose');

const consommableSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nom: { type: String, required: true },
});

module.exports = mongoose.model('consommable', consommableSchema);