const mongoose = require('mongoose');

const incidentSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    // motif: { type: String, required: true },
    // idUser: { type: String, required: true },
    // dateDébut: { type: String, required: true },
    // dateFin: { type: String, required: true },
    image: { type: Array, required: true },
    details : { type: String, required: true},
    localisation : { type: String, required: true},
    idChantier : { type: String, required: true},
    nom : { type: String, required: true},
    prenom : { type: String, required: true},
    date : { type: String, required: true},
    archive : { type: String, required: true},
});

module.exports = mongoose.model('incident', incidentSchema);