const mongoose = require('mongoose');

const drListeUserSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    userId: { type: String, required: true },
    drId: { type: String, required: true },
    userPrenom: { type: String, required: true },
    userNom: { type: String, required: true },

});

module.exports = mongoose.model('drListeUser', drListeUserSchema);