const mongoose = require('mongoose');

const dMaterielSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    items: { type: Object, required: true },
    userId: { type : String, required : true},        
    userNom: { type : String, required : true},        
    userPrenom: { type : String, required : true},        
    idChantier: { type: String, required : true },
    date: { type: String, required : true },
    urgent : { type: Boolean, required : true},
    archive : { type: String, required : true},
});

module.exports = mongoose.model('dmateriel', dMaterielSchema);