const mongoose = require('mongoose');

const currentMissionSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: { type: String, required: true },
    posLat: { type: Number, required: true },
    posLon: { type: Number, required: true },
    hDebut: { type: String, required: true },
    type: { type: String, required: true },
    userId: { type : String, required : true},              
    idChantier: { type: String, required : true },
    pProcess: { type: String, required : true },
});

module.exports = mongoose.model('currentMission', currentMissionSchema);