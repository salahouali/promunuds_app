const mongoose = require('mongoose');

const dCongePayeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: { type: String, required: true },
    userId: { type : String, required : true},        
    userNom: { type : String, required : true},        
    userPrenom: { type : String, required : true},        
    dates: { type: Array, required : true },
    nbjour: { type: Number, required : true },
    raison: { type: String, required : true },
    archive: { type: String, required : true },
    contact : {type: String, required: true}
    
});

module.exports = mongoose.model('dCongePaye', dCongePayeSchema);