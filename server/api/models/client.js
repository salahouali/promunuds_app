const mongoose = require('mongoose');

const clientSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    id: { type: String, required: true },
    name: { type: String, required: true },
    adresse: { type: String, required: true },
    lat: { type: Number, required: true },
    lng: { type: Number, required: true },

});

module.exports = mongoose.model('client', clientSchema);