const mongoose = require('mongoose');

const lingeSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    nom: { type: String, required: true },
});

module.exports = mongoose.model('linge', lingeSchema);