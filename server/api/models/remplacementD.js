const mongoose = require('mongoose');

const dRemplacementSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    date: { type: Array, required: true },
    hDebut: { type: String, required: true },
    hFin: { type: String, required: true },
    userId: { type : String, required : true},        
    userNom: { type : String, required : true},        
    userPrenom: { type : String, required : true},        
    idChantier: { type: String, required : true },
    nbVolontaire: { type: Number, required : true },
    archive: { type: String, required : true },
    
});

module.exports = mongoose.model('dRemplacement', dRemplacementSchema);