// [START initialize_firebase_in_sw]
// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-messaging.js');
// Initialize the Firebase app in the service worker by passing in the messagingSenderId.
firebase.initializeApp({
  apiKey: 'AIzaSyBq7EPfYKSSZz6fcxcwKWNrmf8u5gqb4mA',
  authDomain: 'promundusapp.firebaseapp.com',
  databaseURL: 'https://promundusapp.firebaseio.com',
  projectId: 'promundusapp',
  storageBucket: 'promundusapp.appspot.com',
  messagingSenderId: '1043722405147',
  appId: '1:1043722405147:web:7b49860a6ea33d81287fbe',
  measurementId: 'G-0J5GLQG93D',
  messagingSenderId: '1043722405147',
 // 4. Get Firebase Configuration
});

// Retrieve an instance of Firebase Messaging so that it can handle background messages.
const messaging = firebase.messaging();
// [END initialize_firebase_in_sw]
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: './img/icons/android-chrome-192x192.png'
  };

  return self.registration.showNotification(notificationTitle,
    notificationOptions);
});

