![esi_logo](https://www.esi-metz.fr/wp-content/uploads/sites/48/2017/05/logo-esi-transparent.png) ![promundus_logo](https://www.promundus.fr/services-proprete/wp-content/uploads/2020/06/logo-promundus.png) 

## Promundus APP [ ESI METZ ]

![version](https://img.shields.io/badge/version-0.1-blue.svg)

Application métier qui prend position au coeur des processus applicatif.

## Comment utiliser le projet

```
- Lancer `npm install` ou `yarn install`
- Lancer `npm run dev` ou `yarn serve` pour créer un serveur local
```

D'autres actions possible

```
- `npm run build` pour build l'app
- `npm run lint` pour lancer linting
```

## Développeur

- Salaheddine OUALI

## Référent formation

- Frédéric MULLER