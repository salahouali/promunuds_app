const { GenerateSW } = require('workbox-webpack-plugin')

module.exports = {
  lintOnSave: false,
  devServer: {
    disableHostCheck: true,
    compress: true,
    overlay: {
       warnings: false,
      errors: false,
    },
  },
  transpileDependencies: ['vuetify'],
  // Contourner la limite qui empêche le build
  configureWebpack: {
    performance: { hints: false },
    optimization: {
      splitChunks: {
        minSize: 10000,
        maxSize: 500000,
      },
    },
    plugins: [new GenerateSW()],
  },
  pluginOptions: {
    i18n: {
      locale: 'fr',
      fallbackLocale: 'fr',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
  pwa: {
    name: 'Promundus Application',
    themeColor: '#000000',
    msTileColor: '#000000',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black',

    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      // swSrc is required in InjectManifest mode.
      swSrc: 'src/registerServiceWorker.js',
      // ...other Workbox options...
    },
  },

}
